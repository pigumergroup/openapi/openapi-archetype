Project creation
================

# Build and Install

```
$ mvn clean install
```

# Usage

```
$ mvn archetype:generate \
         -DarchetypeGroupId=com.gitlab.pigumergroup.tools \
         -DarchetypeArtifactId=openapi-archetype \
         -DarchetypeVersion=0.1.0-SNAPSHOT \
         -DgroupId=com.pigumer.example-service \
         -DartifactId=example-service-generated-modules \
         -DserviceId=example \
         -Dversion=0.1.0-SNAPSHOT \
         -Dpackage=com.pigumer.example
```
